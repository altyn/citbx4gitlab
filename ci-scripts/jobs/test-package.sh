
citbx_use runshell

CITBX_PACKAGE_NAME="ci-toolbox"

job_define() {
    CITBX_UID=0
}

job_setup() {
    CI_COMMIT_REF_NAME=$(git rev-parse --abbrev-ref HEAD)
    TEST_USER_UID=$(id -u)
    TEST_USER_HOME=$HOME
    citbx_export CI_COMMIT_REF_NAME TEST_USER_UID TEST_USER_HOME
    citbx_docker_run_add_args --privileged
}

job_main() {
    case "$CI_JOB_NAME" in
        test-package-rpm-*)
            dnf install -y sudo psmisc
            dnf install -y ./artifacts/ci-toolbox.rpm
        ;;
        test-package-deb-*)
            apt update
            apt -y install sudo psmisc
            apt -y install ./artifacts/ci-toolbox.deb
        ;;
    esac
    case "$CI_JOB_NAME" in
        *-rel-*)
            export CITBX_PACKAGE_CHANNEL=rel
        ;;
        *-dev-*)
            export CITBX_PACKAGE_CHANNEL=dev
        ;;
    esac
    if [ -z "$TEST_USER_UID" ]; then
        TEST_USER_UID=1000
    fi
    if [ -z "$TEST_USER_HOME" ]; then
        TEST_USER_HOME=/home/testuser
    fi

    print_info "### STEP1 ### Environment setup"
    useradd -o -u $TEST_USER_UID -s /bin/sh -d $TEST_USER_HOME -M test-user
    echo "test-user ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
    if [ ! -d "$TEST_USER_HOME" ]; then
        mkdir -p "$TEST_USER_HOME"
        chown test-user:test-user "$TEST_USER_HOME"
    fi

    print_info "### STEP2 ### ci-toolbox setup"
    sudo -u test-user env HOME="$TEST_USER_HOME" \
        ci-toolbox setup "$CI_COMMIT_REF_NAME"   \
        --docker-storage-driver overlay2         \
        --docker-bip "172.30.0.1/24"             \
        --docker-cdir "172.30.0.0/24"            \
        --docker-dns 9.9.9.9                     \
        --component base-pkgs                    \
        --component docker-cfg                   \
        --component git-lfs                      \
        --component ca-certs

    print_info "### STEP3 ### Docker daemon start"
    dockerd --data-root "$CI_PROJECT_DIR/testdir/docker/data"&
    echo -n "Waiting for docker daemon"
    until [ -e /var/run/docker.sock ]; do sleep 1; echo -n "."; done

    print_info "### STEP4 ### run pipeline"
    sudo -u test-user env \
        CITBX_SEARCH_PATH=ci-toolbox/ \
        CI_CONFIG_PATH="tests/.gitlab-ci.yml" \
        CITBX_PIPELINES_CONFIG_PATH="tests/.ci-pipelines.yml" \
        CITBX_SCRIPTS_DIR=tests/ci-scripts \
        ci-toolbox pipeline

    print_info "### STEP5 ### run testsuite"
    sudo -u test-user env HOME="$TEST_USER_HOME" ./tests/run-testsuite.sh
}

job_after() {
    if [ -f /var/run/docker.pid ]; then
        # Kill the docker daemon
        local pid=$(cat /var/run/docker.pid)
        kill $pid || true
        wait $pid || true
    fi
    # Remove testdir and ignore errors
    rm -rf testdir || true
}
